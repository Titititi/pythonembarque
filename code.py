from adafruit_circuitplayground.express import cpx
import math
import time

# Etat de départ des LEDs : éteintes
cpx.pixels.brightness = 0.01
cpx.pixels.fill((0, 0, 0))
cpx.pixels.show()

# Définitions des couleurs
VALIDATION_C = (15, 230, 115)

# Définitions des contantes des niveaux de précision souhaité
rayonCercleLEDs = 9.82
Z_MIN = 9               # Equivaut à l'horizontalité maximale du plan du CircuitPlayground (X/9.81)
RED_LIGHT_OK = 0.95     # Equivaut à 5% d'inclinaison par rapport à l'horizon
CLOSE_TO_HORIZ = 0.90   # Equivaut à 10% d'inclinaison par rapport à l'horizon
CLOSE_TO_VERTI = 0.10

# Définition de la constante de correspondance des LEDs (les unes en face des autres)
LED_CORRESPONDANCE = 5

# Définition du tableau de seuils valeurs des LEDs
# La valeur de l'angle varie entre 0 et 2.90 (fournie par Math.acos)
X_MAX = 2.9
SEG_LED = X_MAX / 6                     # = 0.48
# De la LED 2 à 7, dans le sens horaire :
ANG_LED_2 = SEG_LED / 2                 # = 0.24
ANG_LED_1 = ANG_LED_2 + SEG_LED         # = 0.72
ANG_LED_0 = ANG_LED_1 + SEG_LED         # = 1.21
ANG_LED_9 = ANG_LED_0 + 2 * SEG_LED     # = 2.17
ANG_LED_8 = ANG_LED_9 + SEG_LED         # = 2.66
ANG_LED_7 = X_MAX


def switchLights(abs_x, angX):
  cpx.red_led = False
  cpx.pixels.fill((0, 0, 0))
  cpx.pixels.show()

  if CLOSE_TO_HORIZ < abs_x:
    cpx.pixels[0] = VALIDATION_C
    cpx.pixels[9] = VALIDATION_C
    cpx.pixels[4] = VALIDATION_C
    cpx.pixels[5] = VALIDATION_C
    if RED_LIGHT_OK < abs_x:
      cpx.red_led = True
  elif angX <= ANG_LED_2:
    cpx.pixels[2] = VALIDATION_C
    cpx.pixels[2+LED_CORRESPONDANCE] = VALIDATION_C
  elif angX <= ANG_LED_1:
    cpx.pixels[1] = VALIDATION_C
    cpx.pixels[1+LED_CORRESPONDANCE] = VALIDATION_C
  elif angX <= ANG_LED_0 :
    cpx.pixels[0] = VALIDATION_C
    cpx.pixels[LED_CORRESPONDANCE] = VALIDATION_C
  elif angX <= ANG_LED_9:
    cpx.pixels[9] = VALIDATION_C
    cpx.pixels[(9+LED_CORRESPONDANCE)%10] = VALIDATION_C
  elif angX <= ANG_LED_8:
    cpx.pixels[8] = VALIDATION_C
    cpx.pixels[(8+LED_CORRESPONDANCE)%10] = VALIDATION_C
  else:
    cpx.pixels[2] = VALIDATION_C
    cpx.pixels[2+LED_CORRESPONDANCE] = VALIDATION_C
  cpx.pixels.show()


i=0
while True:
  time.sleep(0.1)

  # 0. Update niveau de précision
  if cpx.button_a and RED_LIGHT_OK < 99:
    cpx.play_tone(500, 0.2)
    RED_LIGHT_OK = RED_LIGHT_OK + 1
  if cpx.button_b and 91 < RED_LIGHT_OK:
    cpx.play_tone(650, 0.2)
    RED_LIGHT_OK = RED_LIGHT_OK - 1

  # 1. Récupérer les données
  x_acc, y_acc, z_acc = cpx.acceleration
  abs_z = abs(z_acc)

  if Z_MIN < abs_z:
    print("Le plan du disque est trop horital ou vous agitez trop votre CircuitPlayground Monsieur. :D")
    cpx.pixels.fill((0, 255, 255))
    cpx.pixels.show()

  else:
    x_acc = x_acc / rayonCercleLEDs
    if 1.0 < x_acc: x_acc = 1.0
    elif x_acc < -1.0: x_acc = -1.0
    y_acc = y_acc / rayonCercleLEDs
    if 1.0 < y_acc: y_acc = 1.0
    elif y_acc < -1.0: y_acc = -1.0

    if 0 < y_acc: x_acc = -x_acc

    # 2. Récupérer la force en X
    abs_x = abs(x_acc)

    # 3. En déduire l'angle dans une moitié de cercle
    angCentre = math.acos(x_acc) # puisque rapporté à 1
    angCentre = (angCentre - (X_MAX / 2)) % X_MAX # transformation d'un quart de cercle horaire

    # 4. Allumer les lumières correspondantes
    switchLights(abs_x, angCentre)

    print("---")
    absAng = "{:.2f}".format(angCentre)
    print("ang="+absAng)
    f_x = "{:.2f}".format(x_acc)
    f_y = "{:.2f}".format(y_acc)
    f_z = "{:.2f}".format(z_acc)
    print("x="+f_x)
    print("y="+f_y)
    print("z="+f_z)
    abs_x = "{:.2f}".format(abs_x)
    print("abX="+abs_x)

