# pythonEmbarque

Réalisation d'un niveau de bricolage avec CircuitPlayground

Le CircuitPlayground (CP) indique la position de l'horizon, peu importe l'orientation du CP.
Lorsque le CP s'approche de l'horizon, les 4 LEDs extrèmes (0, 9, 5 et 4) s'allument.

Lorsque le CP est orienté suffisemment parrallèlement à l'horizon, la diode rouge s'allume.
Le seuil d'exigeance de "parrallèlisme à l'horizon" est ajustable
    avec les bouttons A pour le diminuer
    et B pour l'augmenter.
Le seuil va d'une inclinaison par rapport à l'horizon de 1% à 9%.

    // Note : le boutton B ne fonctionnait pas sur mon CP, on dirait.

Attention à ne pas trop le secouer ! ;)

AMELIORATIONS :
    - Faire que la luminosité des LEDs varie en fonction de leur proximité par rapport à l'horizon
    - Commenter encore plus le code
    